<?php

class GumballMachine{

    private $gumballs;

    /**
     * @return mixed
     */
    public function getGumballs()
    {
        return $this->gumballs;
    }

    /**
     * @param mixed $gumballs
     */
    public function setGumballs($gumballs)
    {
        $this->gumballs = $gumballs;
    }

    public function turnWheel(){
        $this->setGumballs($this->getGumballs() - 1);
    }
}