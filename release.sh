#!/bin/bash

#Some test block
sometxt=`git rev-parse HEAD`
echo "This is some text" > "$sometxt.txt"
git add --all
git commit -am "Testing bash $sometxt"
git push

#Configuration
h_master="/var/www/html/h-master" #Folder path for the hash-master where it will store all the commits
c_master="/var/www/html/common-master" #Folder path for the common-master where the last hash will be shown  

#Obtain the git HEAD
git_head=`git rev-parse HEAD` 

#Make the new version folder with the name of the HEAD and copy all the files from it
mkdir "$h_master/$git_head/"
cp -r "." "$h_master/$git_head" 

#Clearing the common folder
rm -r "$c_master"/*

#Symlinks it to the common folder
ln -sf "$h_master/$git_head"/* $c_master

#Clearing out the test block
rm "$sometxt.txt"

echo "DONE!!!"
